const NO_TIME_LEFT = 0
const ONE_MINUTE =  1
const INITIAL_TIME_LEFT = 25

class Pomodoro {
  constructor(bus) {
    this.bus = bus
    this.timeLeft = INITIAL_TIME_LEFT
    this._subscribe()
  }

  _subscribe() {
    this.bus.subscribe('timer.start_requested', this.start.bind(this))
    this.bus.subscribe('timer.askTimeLeft', this.calculateTimeLeft.bind(this))
  }

  start() {
    this.bus.publish('timer.start', this.message())
  }

  calculateTimeLeft() {
    if (this.hasTimeLeft()) {this.minuteCountDown()}
    this.bus.publish('timer.timeLeft', this.message())
  }
  
  hasTimeLeft() {
    return (this.timeLeft > NO_TIME_LEFT)
  }

  minuteCountDown() {
    this.timeLeft -= ONE_MINUTE
  }

  message() {
    return {minutes: this.timeLeft}
  }
}

export default Pomodoro
