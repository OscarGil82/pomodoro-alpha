const ENABLE_BUTTON = `<button id="start">Start</button>`
const DISABLE_BUTTON = `<button id="start" disabled>Start</button>`
const NO_TIME_LEFT = 0


class Timer {
  constructor(document) {
    this.document = document
  }

  render(properties) {
    const minutes = properties.minutes

    return `
      <div id="container">
        <div id="buttons">
          ${this._startButton(minutes)}
        </div>
        <div id="timer">
          <div id="time">
            <span id="minutes">${this._format(minutes)}</span>
          </div>
          <div id="filler"></div>
        </div>
      </div>
    `
  }

  addCallbacks(callbacks) {
    this._addOnClickToStart(callbacks.startCountDown)
  }

  _format(units) {
   var units_string = units.toString()
   if (units_string.length == 1) {
    units_string = "0" + units_string
  }
  return units_string
  }

  _startButton(countDown) {
    let button = DISABLE_BUTTON
    if (countDown == NO_TIME_LEFT) { button = ENABLE_BUTTON }
    return button;
  }

  _addOnClickToStart(callback) {
    const start = this.document.querySelector('#start')
    start.onclick = callback
  }
}

export default Timer
