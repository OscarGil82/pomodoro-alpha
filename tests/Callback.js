
class Callback {
  constructor(bus, topic) {
    this.payload = null

    bus.subscribe(topic, this.storeInformation.bind(this))
  }

  storeInformation(payload) {
    this.payload = payload
  }

  hasBeenCalledWith() {
    return this.payload
  }
}

export default Callback
