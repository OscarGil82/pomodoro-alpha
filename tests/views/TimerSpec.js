import Timer from '../../js/views/Timer.js'

describe('Timer', () => {
  it('draws the minutes in properties', () => {
    const timeLeft = 22
    const properties = { minutes: timeLeft }
    const timer = new Timer()

    const template = timer.render(properties)

    const minutesInProperties = timeLeft.toString()
    expect(template).toContain(minutesInProperties)
  })

  it('draws minutes with two digits', () => {
    const properties = { minutes: 1 }
    const twoDigits = '01'
    const timer = new Timer()

    const template = timer.render(properties)

    expect(template).toContain(twoDigits)
  })

  it('draws enabled start button when the timer does not have time left', () => {
    const properties = { minutes: 0 }
    const enabledStartButton = '<button id="start">Start</button>'
    const timer = new Timer()

    const template = timer.render(properties)

    expect(template).toContain(enabledStartButton)
  })

  it('draws disabled start button when the timer is running', () => {
    const properties = { minutes: 24 }
    const disabledStartButton = '<button id="start" disabled>Start</button>'
    const timer = new Timer()

    const template = timer.render(properties)

    expect(template).toContain(disabledStartButton)
  })
})
